package com.hnu.highlynewapp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class PeopleRequireEachPositionEntity implements Serializable {

    String name;

    Integer value;
}
