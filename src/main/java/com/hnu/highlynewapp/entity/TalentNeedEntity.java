package com.hnu.highlynewapp.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 人才需求
 * 
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:56
 */
@Data
@TableName("talent_need")
public class TalentNeedEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long talentNeedId;
	/**
	 * 职位工作
	 */
	private String position;
	/**
	 * 发布时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
	private Date publishTime;
	/**
	 * 位置（省市县）
	 */
	private String location;
	/**
	 * 详细地址
	 */
	private String address;
	/**
	 * 联系人id
	 */
	private Long contactId;
	/**
	 * 工作内容
	 */
	private String job;
	/**
	 * 职位要求
	 */
	private String demand;
	/**
	 * 期望最低工资
	 */
	private double salaryMin;
	/**
	 * 期望最高工资
	 */
	private double salaryMax;
	/**
	 * 联系电话
	 */
	private String phone;
	/**
	 * 公司网址
	 */
	private String netAddress;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 公司名
	 */
	private String companyId;
	/**
	 * 标题1
	 */
	private String title1;
	/**
	 * 标题2
	 */
	private String title2;
	/**
	 * 标题3
	 */
	private String title3;
	/**
	 * 最低学历
	 */
	private Integer degree;

	/**
	 * 详情图片
	 */
	private String image;

	private Long userId;
	/**
	 * 高校审核
	 */
	private String schoolCheck;
	/**
	 * 学历
	 */
	private String education;
	@TableField(fill = FieldFill.INSERT)
	private Date gmtCreate;

	@TableField(fill = FieldFill.INSERT_UPDATE)
	private Date gmtModified;

	@TableField(fill = FieldFill.INSERT)
	private Integer deleted;

}
