package com.hnu.highlynewapp.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * 人才供应
 * 
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:56
 */
@Data
@TableName("talent_supply")
public class TalentSupplyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId
	private Long talentSupplyId;
	/**
	 * 发布时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
	private Date publishTime;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 照片
	 */
	private String image;

	/**
	 * 年龄
	 */
	private Integer age;
	/**
	 * 专业
	 */
	private String profession;
	/**
	 * 性别 [1:男，2：女]
	 */
	private Integer gender;
	/**
	 * 求职意向
	 */
	private String job;
	/**
	 * 联系人id
	 */
	private Long contactId;
	/**
	 * 专业技能
	 */
	private String skill;
	/**
	 * 个人经历
	 */
	private String experience;
	/**
	 * 学历
	 */
	private Integer degree;
	/**
	 * 入学时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
	private Date enterDate;
	/**
	 * 毕业时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
	private Date graduateDate;
	/**
	 * 学校
	 */
	private String  school;
	/**
	 * 上传简历
	 */
	private String resumeUrl;
	/**
	 * 期望最低工资
	 */
	private double salaryMin;
	/**
	 * 期望最高工资
	 */
	private double salaryMax;
	/**
	 * 工作性质
	 */
	private String jobNature;
	/**
	 * 求职状态
	 */
	private String jobStatus;
	/**
	 * 性格品质
	 */
	private String quality;
	/**
	 * 电话号码
	 */
	private String phone;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 高校审核
	 */
	private String schoolCheck;
}
