package com.hnu.highlynewapp.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 *
 * @author LengKuBaiCai
 * @Date 
 */

@Data
@TableName("financial_institution")
@ApiModel("金融机构")
public class FinancialInstitutionEntity implements Serializable {

  @TableId
  @ApiModelProperty("企业id")
  private long financialInstitutionId;
  @ApiModelProperty("法人id")
  private long legalPersonId;
  @ApiModelProperty("法人姓名")
  private String legalPersonName;
  @ApiModelProperty("企业地址（省市区）")
  private String briefAddress;
  @ApiModelProperty("企业地址（详细地址）")
  private String detailedAddress;
  @ApiModelProperty("经营范围")
  private String businessScope;
  @ApiModelProperty("简介")
  private String briefIntroduction;

  @ApiModelProperty("企业成立日期")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
  private java.sql.Timestamp establishmentDay;
  @ApiModelProperty("企业经营限期")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
  private java.sql.Timestamp timeLimit;
  @ApiModelProperty("审核结果")
  private String result;
  @ApiModelProperty("金融机构网站")
  private String websites;
  @ApiModelProperty("邮箱")
  private String email;
  @ApiModelProperty("注册资本")
  private String registeredCapital;
  @ApiModelProperty("社会信用代码")
  private String socialCreditCode;
  @ApiModelProperty("登记机关")
  private String registrationAuthority;

  @ApiModelProperty("核准日期")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
  private java.sql.Timestamp sanctionDay;
  @ApiModelProperty("固定电话")
  private String fixedLineTelephone;
  @ApiModelProperty("金融机构图片")
  private String financialInstitutionImage;

  @ApiModelProperty("金融机构名字")
  private String financialInstitutionName;

  @ApiModelProperty("企业信用材料")
  private String creditMaterials;

  @ApiModelProperty("审核结果(audit result)，（0-正在审核中，1-通过，2-不通过）")
  private int auditFlag;

  public void setAuditFlag(int auditFlag) {
    this.auditFlag = auditFlag;
  }

  public int getAuditFlag() {
    return auditFlag;
  }


  public String getFinancialInstitutionName() {
    return financialInstitutionName;
  }

  public void setFinancialInstitutionName(String financialInstitutionName) {
    this.financialInstitutionName = financialInstitutionName;
  }

  public String getCreditMaterials() {
    return creditMaterials;
  }

  public void setCreditMaterials(String creditMaterials) {
    this.creditMaterials = creditMaterials;
  }


  public long getFinancialInstitutionId() {
    return financialInstitutionId;
  }

  public void setFinancialInstitutionId(long financialInstitutionId) {
    this.financialInstitutionId = financialInstitutionId;
  }


  public long getLegalPersonId() {
    return legalPersonId;
  }

  public void setLegalPersonId(long legalPersonId) {
    this.legalPersonId = legalPersonId;
  }


  public String getLegalPersonName() {
    return legalPersonName;
  }

  public void setLegalPersonName(String legalPersonName) {
    this.legalPersonName = legalPersonName;
  }


  public String getBriefAddress() {
    return briefAddress;
  }

  public void setBriefAddress(String briefAddress) {
    this.briefAddress = briefAddress;
  }


  public String getDetailedAddress() {
    return detailedAddress;
  }

  public void setDetailedAddress(String detailedAddress) {
    this.detailedAddress = detailedAddress;
  }


  public String getBusinessScope() {
    return businessScope;
  }

  public void setBusinessScope(String businessScope) {
    this.businessScope = businessScope;
  }


  public String getBriefIntroduction() {
    return briefIntroduction;
  }

  public void setBriefIntroduction(String briefIntroduction) {
    this.briefIntroduction = briefIntroduction;
  }


  public java.sql.Timestamp getEstablishmentDay() {
    return establishmentDay;
  }

  public void setEstablishmentDay(java.sql.Timestamp establishmentDay) {
    this.establishmentDay = establishmentDay;
  }


  public java.sql.Timestamp getTimeLimit() {
    return timeLimit;
  }

  public void setTimeLimit(java.sql.Timestamp timeLimit) {
    this.timeLimit = timeLimit;
  }


  public String getResult() {
    return result;
  }

  public void setResult(String result) {
    this.result = result;
  }


  public String getWebsites() {
    return websites;
  }

  public void setWebsites(String websites) {
    this.websites = websites;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public String getRegisteredCapital() {
    return registeredCapital;
  }

  public void setRegisteredCapital(String registeredCapital) {
    this.registeredCapital = registeredCapital;
  }


  public String getSocialCreditCode() {
    return socialCreditCode;
  }

  public void setSocialCreditCode(String socialCreditCode) {
    this.socialCreditCode = socialCreditCode;
  }


  public String getRegistrationAuthority() {
    return registrationAuthority;
  }

  public void setRegistrationAuthority(String registrationAuthority) {
    this.registrationAuthority = registrationAuthority;
  }


  public java.sql.Timestamp getSanctionDay() {
    return sanctionDay;
  }

  public void setSanctionDay(java.sql.Timestamp sanctionDay) {
    this.sanctionDay = sanctionDay;
  }


  public String getFixedLineTelephone() {
    return fixedLineTelephone;
  }

  public void setFixedLineTelephone(String fixedLineTelephone) {
    this.fixedLineTelephone = fixedLineTelephone;
  }


  public String getFinancialInstitutionImage() {
    return financialInstitutionImage;
  }

  public void setFinancialInstitutionImage(String financialInstitutionImage) {
    this.financialInstitutionImage = financialInstitutionImage;
  }

}
