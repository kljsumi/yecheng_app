package com.hnu.highlynewapp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnu.highlynewapp.entity.FinancialInstitutionEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 *
 * @author LengKuBaiCai
 * @Date
 */

@Mapper
public interface FinancialInstitutionDao extends BaseMapper<FinancialInstitutionEntity> {

}
