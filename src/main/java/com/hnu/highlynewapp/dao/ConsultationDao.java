package com.hnu.highlynewapp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnu.highlynewapp.entity.ConsultationEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 咨询
 * 
 * @author kLjSumi
 * @email 825963704@qq.com
 * @date 2021-04-08 20:12:22
 */
@Mapper
public interface ConsultationDao extends BaseMapper<ConsultationEntity> {
	
}
