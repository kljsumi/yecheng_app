package com.hnu.highlynewapp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnu.highlynewapp.entity.InvestingNeedEntity;
import org.apache.ibatis.annotations.Mapper;


/**
 *
 * @author LengKuBaiCai
 * @Date
 */

@Mapper
public interface InvestingNeedDao extends BaseMapper<InvestingNeedEntity> {

}
