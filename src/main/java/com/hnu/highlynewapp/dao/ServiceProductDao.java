package com.hnu.highlynewapp.dao;

import com.hnu.highlynewapp.entity.ServiceProductEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 服务产品
 * 
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:56
 */
@Mapper
public interface ServiceProductDao extends BaseMapper<ServiceProductEntity> {
	
}
