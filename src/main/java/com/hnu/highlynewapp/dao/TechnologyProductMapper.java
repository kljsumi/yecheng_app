package com.hnu.highlynewapp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnu.highlynewapp.transformation.pojo.TechnologyProductEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface TechnologyProductMapper extends BaseMapper<TechnologyProductEntity> {
}
