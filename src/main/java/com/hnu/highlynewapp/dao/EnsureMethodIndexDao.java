package com.hnu.highlynewapp.dao;

import com.hnu.highlynewapp.entity.EnsureMethodIndexEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 担保方式检索
 * 
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-04 22:01:47
 */
@Mapper
public interface EnsureMethodIndexDao extends BaseMapper<EnsureMethodIndexEntity> {
	
}
