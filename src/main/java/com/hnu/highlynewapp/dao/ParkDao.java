package com.hnu.highlynewapp.dao;

import com.hnu.highlynewapp.entity.ParkEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-01-24
 */
@Mapper
public interface ParkDao extends BaseMapper<ParkEntity> {

}
