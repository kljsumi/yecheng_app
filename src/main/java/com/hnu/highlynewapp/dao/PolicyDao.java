package com.hnu.highlynewapp.dao;

import com.hnu.highlynewapp.entity.PolicyEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 政策
 * 
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:56
 */
@Mapper
public interface PolicyDao extends BaseMapper<PolicyEntity> {
	
}
