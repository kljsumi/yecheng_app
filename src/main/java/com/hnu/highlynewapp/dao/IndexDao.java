package com.hnu.highlynewapp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnu.highlynewapp.entity.FinancingNeedEntity;
import com.hnu.highlynewapp.entity.IndexEntity;
import org.mapstruct.Mapper;

/**
 * @author kLjSumi
 * @Date 2021/9/19 16:26
 */
@Mapper
public interface IndexDao extends BaseMapper<IndexEntity> {
}
