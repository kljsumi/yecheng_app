package com.hnu.highlynewapp.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hnu.highlynewapp.entity.InvestFinancialProjectEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface InvestFinancialProjectDao extends BaseMapper<InvestFinancialProjectEntity> {

}
