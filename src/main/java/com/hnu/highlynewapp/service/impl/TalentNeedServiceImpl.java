package com.hnu.highlynewapp.service.impl;

import com.hnu.highlynewapp.entity.*;
import com.hnu.highlynewapp.service.ContactService;
import com.hnu.highlynewapp.vo.PublishTalentNeedVo;
import com.hnu.highlynewapp.vo.TalentNeedSearchVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.utils.Query;

import com.hnu.highlynewapp.dao.TalentNeedDao;
import com.hnu.highlynewapp.service.TalentNeedService;
import org.springframework.util.StringUtils;


@Service("talentNeedService")
public class TalentNeedServiceImpl extends ServiceImpl<TalentNeedDao, TalentNeedEntity> implements TalentNeedService {

    @Autowired
    private ContactService contactService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TalentNeedEntity> page = this.page(
                new Query<TalentNeedEntity>().getPage(params),
                new QueryWrapper<TalentNeedEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void publish(PublishTalentNeedVo vo) {
        ContactEntity contactEntity = new ContactEntity();
        BeanUtils.copyProperties(vo,contactEntity);
        contactEntity.setAddress(vo.getContactAddress());
        contactEntity.setPosition(vo.getContactPosition());
        Long contactId = contactService.saveAndCheckExist(contactEntity);

        TalentNeedEntity talentNeedEntity = new TalentNeedEntity();
        BeanUtils.copyProperties(vo,talentNeedEntity);
        talentNeedEntity.setAddress(vo.getBusinessAddress());
        talentNeedEntity.setPosition(vo.getBusinessPosition());
        talentNeedEntity.setPublishTime(new Date());
        talentNeedEntity.setContactId(contactId);
        baseMapper.insert(talentNeedEntity);
    }

    @Override
    public List<TalentNeedEntity> getListByParma() {

        QueryWrapper<TalentNeedEntity> wrapper = new QueryWrapper<>();

        wrapper.orderByDesc("publish_time");
        List<TalentNeedEntity> list = baseMapper.selectList(wrapper);
        return list;
    }
    @Override
    public List<TalentNeedEntity> listByCondition(String duration, String degree){
        List<TalentNeedEntity> byDuration = new ArrayList<>();
        List<TalentNeedEntity> byDegree = new ArrayList<>();

        int i = 0;
        switch (duration) {
            case "一个月内":
                i = 30;
                break;
            case "三个月内":
                i = 90;
                break;
            case "六个月内":
                i = 180;
                break;
            case "一年内":
                i = 365;
                break;
            default:
                i = -1;
                break;
        }
        List<TalentNeedEntity> result = new ArrayList<>();
        if ("全部".equals(duration)&&"全部".equals(degree)) {
            QueryWrapper<TalentNeedEntity> wrapper= new QueryWrapper<>();
            wrapper.orderByDesc("publish_time");
            result = this.list(wrapper);
        } else if (!"全部".equals(duration)&&"全部".equals(degree)){
            result = this.getBaseMapper().listByDuration(i);
        } else if ("全部".equals(duration)&&!"全部".equals(degree)) {
            result = this.getBaseMapper().listByField(degree);
        } else {
            result = this.getBaseMapper().listByBoth(i,degree);
        }
        return result;
    }
    @Override
    public List<TalentNeedEntity> listByCheckId(Integer check){
        List<TalentNeedEntity> result = new ArrayList<>();
        result = this.getBaseMapper().listCheckId(check);
        return result;
    }
    @Override
    public List<TalentNeedEntity> getConditionList(String condition){
        List<TalentNeedEntity> result = new ArrayList<>();
        result = this.getBaseMapper().listByCondition(condition);
        return result;
    }

    @Override
    public List<PeopleRequireEachPositionEntity> getNumberOfPeopleRequiredEachPosition() {
        List<PeopleRequireEachPositionEntity> result = new ArrayList<>();

        //获得list链表
        QueryWrapper<TalentNeedEntity> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("publish_time");
        List<TalentNeedEntity> list = baseMapper.selectList(wrapper);

        //使用hash对list进行计数
        HashMap<String, Integer> map = new HashMap<>();
        for(int i=0; i<list.size(); i++){
            String pos = list.get(i).getPosition();
            Integer count = map.get(pos);
            if(count==null){
                map.put(pos, 1);
            }else{
                map.put(pos, count+1);
            }
        }

        map.forEach((key,value)->{
            result.add(new PeopleRequireEachPositionEntity(key,value));
        });


        return result;
    }
}
