package com.hnu.highlynewapp.service.impl;

import com.hnu.highlynewapp.entity.ContactEntity;
import com.hnu.highlynewapp.entity.PeopleRequireEachPositionEntity;
import com.hnu.highlynewapp.entity.TalentNeedEntity;
import com.hnu.highlynewapp.service.ContactService;
import com.hnu.highlynewapp.vo.PublishTalentSupplyVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.utils.Query;

import com.hnu.highlynewapp.dao.TalentSupplyDao;
import com.hnu.highlynewapp.entity.TalentSupplyEntity;
import com.hnu.highlynewapp.service.TalentSupplyService;
import org.springframework.transaction.annotation.Transactional;


@Service("talentSupplyService")
public class TalentSupplyServiceImpl extends ServiceImpl<TalentSupplyDao, TalentSupplyEntity> implements TalentSupplyService {

    @Autowired
    private ContactService contactService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<TalentSupplyEntity> page = this.page(
                new Query<TalentSupplyEntity>().getPage(params),
                new QueryWrapper<TalentSupplyEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional
    public void publish(PublishTalentSupplyVo vo) {
        ContactEntity contactEntity = new ContactEntity();
        BeanUtils.copyProperties(vo,contactEntity);
        contactEntity.setName(vo.getContactName());
        Long contactId = contactService.saveAndCheckExist(contactEntity);

        TalentSupplyEntity talentSupplyEntity = new TalentSupplyEntity();
        BeanUtils.copyProperties(vo,talentSupplyEntity);
        talentSupplyEntity.setPublishTime(new Date());
        talentSupplyEntity.setName(vo.getTalentName());
        talentSupplyEntity.setContactId(contactId);
        baseMapper.insert(talentSupplyEntity);
    }
    @Override
    public List<TalentSupplyEntity> getListByParma() {

        QueryWrapper<TalentSupplyEntity> wrapper = new QueryWrapper<>();

        wrapper.orderByDesc("publish_time");
        List<TalentSupplyEntity> list = baseMapper.selectList(wrapper);
        return list;
    }
    @Override
    public List<TalentSupplyEntity> listByCondition(String duration, String degree){
        List<TalentSupplyEntity> byDuration = new ArrayList<>();
        List<TalentSupplyEntity> byDegree = new ArrayList<>();

        int i = 0;
        switch (duration) {
            case "一个月内":
                i = 30;
                break;
            case "三个月内":
                i = 90;
                break;
            case "六个月内":
                i = 180;
                break;
            case "一年内":
                i = 365;
                break;
            default:
                i = -1;
                break;
        }
        List<TalentSupplyEntity> result = new ArrayList<>();
        if ("全部".equals(duration)&&"全部".equals(degree)) {
            QueryWrapper<TalentSupplyEntity> wrapper= new QueryWrapper<>();
            wrapper.orderByDesc("publish_time");
            result = this.list(wrapper);
        } else if (!"全部".equals(duration)&&"全部".equals(degree)){
            result = this.getBaseMapper().listByDuration(i);
        } else if ("全部".equals(duration)&&!"全部".equals(degree)) {
            result = this.getBaseMapper().listByField(degree);
        } else {
            result = this.getBaseMapper().listByBoth(i,degree);
        }
        return result;
    }

    @Override
    public List<TalentSupplyEntity> listByCheckId(Integer check){
        List<TalentSupplyEntity> result = new ArrayList<>();
        result = this.getBaseMapper().listCheckId(check);
        return result;
    }
    @Override
    public List<TalentSupplyEntity> getConditionList(String condition){
        List<TalentSupplyEntity> result = new ArrayList<>();
        result = this.getBaseMapper().listCondition(condition);
        return result;
    }

    @Override
    public List<PeopleRequireEachPositionEntity> getTalentDataPieChartList() {
        List<PeopleRequireEachPositionEntity> result = new ArrayList<>();

        //获得list链表
        QueryWrapper<TalentSupplyEntity> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("publish_time");
        List<TalentSupplyEntity> list = baseMapper.selectList(wrapper);

        //使用hash对list进行计数
        HashMap<String, Integer> map = new HashMap<>();
        for(int i=0; i<list.size(); i++){
            String pos = list.get(i).getJob();
            Integer count = map.get(pos);
            if(count==null){
                map.put(pos, 1);
            }else{
                map.put(pos, count+1);
            }
        }

        map.forEach((key,value)->{
            result.add(new PeopleRequireEachPositionEntity(key,value));
        });


        return result;
    }
}
