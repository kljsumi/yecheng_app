package com.hnu.highlynewapp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnu.highlynewapp.dao.BusinessInfoDao;
import com.hnu.highlynewapp.entity.BusinessInfoEntity;
import com.hnu.highlynewapp.entity.ContactEntity;
import com.hnu.highlynewapp.entity.UserEntity;
import com.hnu.highlynewapp.enums.StatusEnum;
import com.hnu.highlynewapp.service.BusinessInfoService;
import com.hnu.highlynewapp.service.ContactService;
import com.hnu.highlynewapp.service.UserService;
import com.hnu.highlynewapp.transformation.vo.BusinessNeedVo;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.utils.Query;
import com.hnu.highlynewapp.vo.BusinessInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

@Service("businessInfoService")
public class BusinessInfoServiceImpl extends ServiceImpl<BusinessInfoDao, BusinessInfoEntity> implements BusinessInfoService {
    @Autowired
    private BusinessInfoDao businessInfoDao;

    @Autowired
    private ContactService contactService;

    @Autowired
    private UserService userService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<BusinessInfoEntity> page = this.page(
                new Query<BusinessInfoEntity>().getPage(params),
                new QueryWrapper<BusinessInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<BusinessInfoEntity> getNoCheckList() {
        List<BusinessInfoEntity> list = baseMapper.selectList(new QueryWrapper<BusinessInfoEntity>().eq("status", StatusEnum.STATU_ONE.getCode()));
        return list;
    }

    @Override
    @Transactional
    public void passCheck(Long businessId) {
        BusinessInfoEntity businessInfoEntity = this.getById(businessId);
        businessInfoEntity.setStatus(StatusEnum.STATU_TWO.getCode());
        this.updateById(businessInfoEntity);

        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(businessInfoEntity.getUserId());
        userEntity.setType(1);
        userEntity.setStatus(2);
        userService.updateById(userEntity);
    }

    @Override
    @Transactional
    public void auth(BusinessInfoVo businessInfoVo) {
        BusinessInfoEntity businessInfoEntity = new BusinessInfoEntity();
        ContactEntity contactEntity = new ContactEntity();
        BeanUtils.copyProperties(businessInfoVo, businessInfoEntity);
        BeanUtils.copyProperties(businessInfoVo, contactEntity);

        contactEntity.setAddress(businessInfoVo.getContactAddress());
        contactEntity.setEmail(businessInfoVo.getContactEmail());
        contactEntity.setName(businessInfoVo.getContactName());
        contactEntity.setPosition(businessInfoVo.getContactPosition());
        contactEntity.setPhone(businessInfoVo.getContactPhone());
        Long i = contactService.saveAndCheckExist(contactEntity);
        businessInfoEntity.setContactId(i);
        businessInfoEntity.setStatus(StatusEnum.STATU_ONE.getCode());
        this.save(businessInfoEntity);
        UserEntity user = new UserEntity();
        user.setUserId(businessInfoVo.getUserId());
        user.setStatus(1);
        userService.updateById(user);
    }

    @Override
    @Transactional
    public void failCheck(Long businessId) {
        BusinessInfoEntity businessInfoEntity = this.getById(businessId);
        businessInfoEntity.setStatus(StatusEnum.STATU_THREE.getCode());
        this.updateById(businessInfoEntity);
        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(businessInfoEntity.getUserId());
        userEntity.setStatus(0);
        userService.updateById(userEntity);
    }

    /**
     * 获取要进行匹配的企业信息
     *
     * @param vo
     * @return
     */
    @Override
    public BusinessInfoEntity getBusinessDetail(BusinessNeedVo vo) {
        QueryWrapper<BusinessInfoEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", vo.getName());
        queryWrapper.eq("code", vo.getCode());
        return businessInfoDao.selectOne(queryWrapper);
    }
}