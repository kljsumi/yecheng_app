package com.hnu.highlynewapp.service.impl;

import org.springframework.stereotype.Service;

import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.utils.Query;

import com.hnu.highlynewapp.dao.ContactDao;
import com.hnu.highlynewapp.entity.ContactEntity;
import com.hnu.highlynewapp.service.ContactService;

@Service("contactService")
public class ContactServiceImpl extends ServiceImpl<ContactDao, ContactEntity> implements ContactService {
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ContactEntity> page = this.page(
                new Query<ContactEntity>().getPage(params),
                new QueryWrapper<ContactEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public Long saveAndCheckExist(ContactEntity contactEntity) {
        ContactEntity entity = baseMapper.selectOne(new QueryWrapper<ContactEntity>().eq("phone", contactEntity.getPhone()));
        if (entity == null) {
            baseMapper.insert(contactEntity);
            ContactEntity entity1 = baseMapper.selectOne(new QueryWrapper<ContactEntity>().eq("phone", contactEntity.getPhone()));
            return entity1.getContactId();
        } else {
            return entity.getContactId();
        }
    }

}