package com.hnu.highlynewapp.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hnu.highlynewapp.dao.FinancialInstitutionDao;
import com.hnu.highlynewapp.dao.FinancialProductDao;
import com.hnu.highlynewapp.entity.ContactEntity;
import com.hnu.highlynewapp.entity.FinancialInstitutionEntity;
import com.hnu.highlynewapp.entity.FinancialProductEntity;
import com.hnu.highlynewapp.entity.InvestingNeedEntity;
import com.hnu.highlynewapp.service.FinancialInstitutionService;
import com.hnu.highlynewapp.service.FinancialProductService;
import com.hnu.highlynewapp.service.InvestingNeedService;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.utils.Query;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import com.hnu.highlynewapp.vo.FinancialInstitutionVo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

/**
 *
 * @author LengKuBaiCai
 * @Date 
 */
 


@Service("FinancialInstitutionService")
public class FinancialInstitutionServiceImpl extends ServiceImpl<FinancialInstitutionDao, FinancialInstitutionEntity> implements FinancialInstitutionService {

    private FinancialProductService financialProductService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<FinancialInstitutionEntity> page = this.page(
                new Query<FinancialInstitutionEntity>().getPage(params),
                new QueryWrapper<FinancialInstitutionEntity>()
        );
        return new PageUtils(page);
    }

    @Override
    public boolean request(FinancialInstitutionEntity financialInstitutionEntity){
        financialInstitutionEntity = converterFinancialInstitutionEntity(financialInstitutionEntity);
        QueryWrapper<InvestingNeedEntity> wrapper = new QueryWrapper<>();
        int result = baseMapper.insert(financialInstitutionEntity);
        boolean flag = false;
        if(result!=0){
           flag = true;
        }
        return flag;
    }
    private FinancialInstitutionEntity converterFinancialInstitutionEntity(FinancialInstitutionEntity financialInstitutionEntity) {
        if (financialInstitutionEntity.getSanctionDay() == null){
            financialInstitutionEntity.setSanctionDay(new Timestamp(System.currentTimeMillis()));
        }
        return financialInstitutionEntity;
    }

    @Override
    public List<FinancialInstitutionEntity> getListById(long financial_institution_id){
        QueryWrapper<FinancialInstitutionEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("financial_institution_id",financial_institution_id);
        List<FinancialInstitutionEntity> list = baseMapper.selectList(wrapper);
        return list;
    }

    @Override
    public List<FinancialInstitutionEntity> getByFinancialInstitutionName(String financial_institution_name){
        QueryWrapper<FinancialInstitutionEntity> wrapper = new QueryWrapper<>();
        wrapper.like("financial_institution_name",financial_institution_name);
        List<FinancialInstitutionEntity> list = baseMapper.selectList(wrapper);
        return list;
    }

    @Override
    public int updateFinancialInstitution(FinancialInstitutionEntity financialInstitutionEntity){

        int flag = baseMapper.updateById(financialInstitutionEntity);
        return flag;
    }

    @Override
    public String getResultByFinancialInstitutionId(long financial_institution_id){
        FinancialInstitutionEntity financialInstitutionEntity = baseMapper.selectById(financial_institution_id);
        String result = financialInstitutionEntity.getResult();
        return result;
    }
    @Override
    public List<FinancialInstitutionEntity> getCreditMaterialsByFinancialInstitutionName(String financial_institution_name){
        QueryWrapper<FinancialInstitutionEntity> wrapper = new QueryWrapper<>();
        wrapper.like("financial_institution_name",financial_institution_name);
        List<FinancialInstitutionEntity> list = baseMapper.selectList(wrapper);
        return list;
    }


}
