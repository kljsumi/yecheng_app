package com.hnu.highlynewapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnu.highlynewapp.entity.PeopleRequireEachPositionEntity;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.entity.TalentSupplyEntity;
import com.hnu.highlynewapp.vo.PublishTalentSupplyVo;

import java.util.List;
import java.util.Map;

/**
 * 人才供应
 *
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:56
 */
public interface TalentSupplyService extends IService<TalentSupplyEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void publish(PublishTalentSupplyVo vo);

    List<TalentSupplyEntity> getListByParma();

    List<TalentSupplyEntity> listByCondition(String duration, String degree);

    List<TalentSupplyEntity> listByCheckId(Integer check);

    List<TalentSupplyEntity> getConditionList(String condition);

    List<PeopleRequireEachPositionEntity> getTalentDataPieChartList();
}

