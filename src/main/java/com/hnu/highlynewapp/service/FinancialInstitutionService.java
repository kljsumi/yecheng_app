package com.hnu.highlynewapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.hnu.highlynewapp.entity.FinancialInstitutionEntity;
import com.hnu.highlynewapp.entity.FinancialProductEntity;
import com.hnu.highlynewapp.entity.InvestingNeedEntity;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.vo.FinancialInstitutionVo;
import com.hnu.highlynewapp.vo.InvestingSearchByParamVo;

import java.util.List;
import java.util.Map;

/**
 *
 * @author LengKuBaiCai
 * @Date
 */


public interface FinancialInstitutionService extends IService<FinancialInstitutionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    boolean request(FinancialInstitutionEntity financialInstitutionEntity);

    List<FinancialInstitutionEntity> getListById(long financial_institution_id);

    List<FinancialInstitutionEntity> getByFinancialInstitutionName(String financial_institution_name);

    int updateFinancialInstitution(FinancialInstitutionEntity financialInstitutionEntity);
    String  getResultByFinancialInstitutionId(long financial_institution_id);

    List<FinancialInstitutionEntity> getCreditMaterialsByFinancialInstitutionName(String financial_institution_name);
}