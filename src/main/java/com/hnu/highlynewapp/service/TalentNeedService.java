package com.hnu.highlynewapp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hnu.highlynewapp.entity.PeopleRequireEachPositionEntity;
import com.hnu.highlynewapp.entity.TalentSupplyEntity;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.entity.TalentNeedEntity;
import com.hnu.highlynewapp.vo.PublishTalentNeedVo;
import com.hnu.highlynewapp.vo.TalentNeedSearchVo;

import java.util.List;
import java.util.Map;

/**
 * 人才需求
 *
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:56
 */
public interface TalentNeedService extends IService<TalentNeedEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void publish(PublishTalentNeedVo vo);

    List<TalentNeedEntity> getListByParma();

    List<TalentNeedEntity> listByCondition(String duration, String degree);

    List<TalentNeedEntity> listByCheckId(Integer check);

    List<TalentNeedEntity> getConditionList(String condition);

    //统计每个岗位所需要的人数
    List<PeopleRequireEachPositionEntity> getNumberOfPeopleRequiredEachPosition();
}

