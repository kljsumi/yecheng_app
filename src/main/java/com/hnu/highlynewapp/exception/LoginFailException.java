package com.hnu.highlynewapp.exception;

/**
 * 登录时用户名或密码错误抛出此异常
 */
public class LoginFailException extends Exception {
    private int code = 722;
    private String message = "用户名或密码错误";

    public LoginFailException() {
        super();
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
