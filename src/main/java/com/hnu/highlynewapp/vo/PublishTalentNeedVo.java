package com.hnu.highlynewapp.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * @author kLjSumi
 * @Date 2021/1/6
 */
@ApiModel("发布人才需求请求参数")
@Data
public class PublishTalentNeedVo {
    /**
     * 职位工作
     */
    @ApiModelProperty(value = "职位工作",required = true)
    @NotBlank(message = "职位工作不能为空")
    private String businessPosition;
    /**
     * 位置（省市县）
     */
    @ApiModelProperty(value = "位置（省市县）",required = true)
    @NotBlank(message = "位置不能为空")
    private String location;
    /**
     * 详细地址
     */
    @ApiModelProperty(value = "详细地址",required = true)
    @NotBlank(message = "详细地址不能为空")
    private String businessAddress;
    /**
     * 工作内容
     */
    @ApiModelProperty(value = "工作内容",required = true)
    @NotBlank(message = "工作内容不能为空")
    private String job;
    /**
     * 职位要求
     */
    @ApiModelProperty(value = "职位要求",required = true)
    @NotBlank(message = "职位要求不能为空")
    private String demand;
    /**
     * 学历
     */
    @ApiModelProperty(value = "学历[1:博士研究生，2：硕士研究生，3：本科，4：大专]",required = true)
    @Max(value = 4,message = "请输入正确学历")
    @NotNull(message = "最低学历要求不能为空")
    private Integer degree;
    /**
     * 详情图片
     */
    @ApiModelProperty(value = "详情图片")
    private String image;

    @ApiModelProperty(value = "学历要求", required = true)
    @NotBlank(message = "学历要求不能为空")
    private String education;
    /**
     * 企业id
     */
    @ApiModelProperty(value = "企业id",required = true)
    @NotNull(message = "企业id不能为空")
    private Long businessId;

    /**
     * 联系人姓名
     */
    @ApiModelProperty(value = "联系人姓名",required = true)
    @NotBlank(message = "联系人姓名不能为空")
    private String name;
    /**
     * 电话
     */
    @ApiModelProperty(value = "联系人电话",required = true)
    @NotBlank(message = "联系人电话不能为空")
    private String phone;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "联系人邮箱",required = true)
    @NotBlank(message = "联系人邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    private String email;
    /**
     * 地址
     */
    @ApiModelProperty("联系人地址")
    private String contactAddress;
    /**
     * 单位
     */
    @ApiModelProperty("联系人单位")
    private String contactPosition;
    /**
     * 期望最低薪资
     */
    @ApiModelProperty("期望最低薪资")
    private Double salaryMin;
    /**
     * 期望最高薪资
     */
    @ApiModelProperty("期望最高薪资")
    private Double salaryMax;
    /**
     * 公司网址
     */
    @ApiModelProperty("网址")
    private String netAddress;
    @ApiModelProperty("公司名")
    private String companyId;
    @ApiModelProperty("标题1")
    private String title1;
    @ApiModelProperty("标题2")
    private String title2;
    @ApiModelProperty("标题3")
    private String title3;

}
