package com.hnu.highlynewapp.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 *
 * @author LengKuBaiCai
 * @Date 
 */
 

@Data
@ApiModel("申请金融机构表认证")
public class FinancialInstitutionVo {
    
    @TableId
    @ApiModelProperty("企业id")
    private long financialInstitutionId;
    @ApiModelProperty("法人id")
    private long legalPersonId;
    @ApiModelProperty("法人姓名")
    private String legalPersonName;
    @ApiModelProperty("企业地址（省市区）")
    private String briefAddress;
    @ApiModelProperty("企业地址（详细地址）")
    private String detailedAddress;
    @ApiModelProperty("经营范围")
    private String businessScope;
    @ApiModelProperty("简介")
    private String briefIntroduction;

    @ApiModelProperty("企业成立日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private java.sql.Timestamp establishmentDay;
    @ApiModelProperty("企业经营限期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private java.sql.Timestamp timeLimit;
    @ApiModelProperty("审核结果")
    private String result;
    @ApiModelProperty("金融机构网站")
    private String websites;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("注册资本")
    private String registeredCapital;
    @ApiModelProperty("社会信用代码")
    private String socialCreditCode;
    @ApiModelProperty("登记机关")
    private String registrationAuthority;

    @ApiModelProperty("核准日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" , timezone = "GMT+8")
    private java.sql.Timestamp sanctionDay;
    @ApiModelProperty("固定电话")
    private String fixedLineTelephone;
    @ApiModelProperty("金融机构图片")
    private String financialInstitutionImage;
    
}
