package com.hnu.highlynewapp.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;
import java.util.Date;

/**
 * @author kLjSumi
 * @Date 2021/1/5
 */
@Data
@ApiModel("发布人才供应请求参数")
public class PublishTalentSupplyVo {
    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名",required = true)
    @NotBlank(message = "姓名不能为空")
    private String talentName;
    /**
     * 照片
     */
    @ApiModelProperty(value = "照片",required = true)
    @NotBlank(message = "照片不能为空")
    @URL(message = "照片url不合法")
    private String image;
    /**
     * 年龄
     */
    @ApiModelProperty(value = "年龄",required = true)
    @Max(value = 200,message = "请输入正确年龄")
    @NotNull(message = "年龄不能为空")
    private Integer age;
    /**
     * 专业
     */
    @ApiModelProperty(value = "专业",required = true)
    @NotBlank(message = "专业不能为空")
    private String profession;
    /**
     * 性别 [1:男，2：女]
     */
    @ApiModelProperty(value = "性别[1:男，2：女]",required = true)
    @Max(value = 2,message = "请输入正确性别")
    @NotNull(message = "性别不能为空")
    private Integer gender;
    /**
     * 求职意向
     */
    @ApiModelProperty(value = "求职意向",required = true)
    @NotBlank(message = "求职意向不能为空")
    private String job;
    /**
     * 专业技能
     */
    @ApiModelProperty(value = "专业技能",required = true)
    @NotBlank(message = "专业技能不能为空")
    private String skill;
    /**
     * 个人经历
     */
    @ApiModelProperty("个人经历")
    private String experience;
    /**
     * 学历
     */
    @ApiModelProperty(value = "学历[1:博士研究生，2：硕士研究生，3：本科，4：大专]",required = true)
    @Max(value = 4,message = "请输入正确学历")
    @NotNull(message = "学历不能为空")
    private Integer degree;
    /**
     * 学校id
     */
    @ApiModelProperty(value = "学校",required = true)
    @NotNull(message = "学校不能为空")
    private String school;
    /**
     * 上传简历
     */
    @ApiModelProperty("上传简历")
    @URL(message = "简历url不合法")
    private String resumeUrl;

    /**
     * 联系人姓名
     */
    @ApiModelProperty(value = "联系人姓名",required = true)
    @NotBlank(message = "联系人姓名不能为空")
    private String contactName;
    /**
     * 电话
     */
    @ApiModelProperty(value = "联系人电话",required = true)
    @NotBlank(message = "联系人电话不能为空")
    private String phone;
    /**
     * 邮箱
     */
    @ApiModelProperty(value = "联系人邮箱",required = true)
    @NotBlank(message = "联系人邮箱不能为空")
    @Email(message = "邮箱格式不正确")
    private String email;
    /**
     * 地址
     */
    @ApiModelProperty("联系人地址")
    private String address;
    /**
     * 单位
     */
    @ApiModelProperty("联系人单位")
    private String position;
    /**
     * 期望最低薪资
     */
    @ApiModelProperty("期望最低薪资")
    private Double salaryMin;
    /**
     * 期望最高薪资
     */
    @ApiModelProperty("期望最高薪资")
    private Double salaryMax;
    /**
     * 工作状态
     */
    @ApiModelProperty("工作性质")
    private String jobNature;
    /**
     * 求职状态
     */
    @ApiModelProperty("求职状态")
    private String jobStatus;
    /**
     * 性格品质
     */
    @ApiModelProperty("性格品质")
    private String quality;
    /**
     * 高校审核
     */
    @ApiModelProperty("高校审核")
    private String schoolCheck;
}
