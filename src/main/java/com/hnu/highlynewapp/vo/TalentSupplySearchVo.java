package com.hnu.highlynewapp.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("检索人才供应信息，不填默认获取全部")
public class TalentSupplySearchVo {
    @ApiModelProperty("名字")
    private String name;
    @ApiModelProperty("照片")
    private String image;
    @ApiModelProperty("年龄")
    private String age;
    @ApiModelProperty("职称")
    private String profession;
    @ApiModelProperty("性别")
    private String gender;
    @ApiModelProperty("工作")
    private String job;
    @ApiModelProperty("联系方式ID")
    private String contact_id;
    @ApiModelProperty("技能")
    private String skill;
    @ApiModelProperty("经历")
    private String experience;
    @ApiModelProperty("学历")
    private Integer degree;
    @ApiModelProperty("入学日期")
    private String enter_date;
    @ApiModelProperty("毕业日期")
    private String graduate_date;
    @ApiModelProperty("学校")
    private String school;
    @ApiModelProperty("概要链接")
    private String resume_url;
    @ApiModelProperty("期望最低薪资")
    private Double salaryMin;
    @ApiModelProperty("期望最高薪资")
    private Double salaryMax;
    @ApiModelProperty("工作性质")
    private Integer jobNature;
    @ApiModelProperty("求职状态")
    private Integer jobStatus;
    @ApiModelProperty("性格品质")
    private String quality;
    @ApiModelProperty("电话号码")
    private String phone;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("高校审核")
    private String schoolCheck;
}
