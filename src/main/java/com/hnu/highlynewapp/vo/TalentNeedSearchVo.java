package com.hnu.highlynewapp.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("检索人才需求信息，不填默认获取全部")
public class TalentNeedSearchVo {
    @ApiModelProperty("职务")
    private String position;
    @ApiModelProperty("工作地")
    private String location;
    @ApiModelProperty("单位地址")
    private String address;
    @ApiModelProperty("联系方式ID")
    private String contact_id;
    @ApiModelProperty("工作")
    private String job;
    @ApiModelProperty("要求")
    private String demand;
    @ApiModelProperty("照片")
    private String image;
    @ApiModelProperty("公司ID")
    private String business_id;
    @ApiModelProperty("期望最低薪资")
    private Double salaryMin;
    @ApiModelProperty("期望最高薪资")
    private Double salaryMax;
    @ApiModelProperty("公司网址")
    private String netAddress;
    @ApiModelProperty("电话")
    private String phone;
    @ApiModelProperty("邮箱")
    private String email;
    @ApiModelProperty("公司名")
    private String companyId;
    @ApiModelProperty("标题2")
    private String title1;
    @ApiModelProperty("标题2")
    private String title2;
    @ApiModelProperty("标题3")
    private String title3;
    @ApiModelProperty("最低学历")
    private Integer degree;
    @ApiModelProperty("高校审核")
    private String schoolCheck;


}
