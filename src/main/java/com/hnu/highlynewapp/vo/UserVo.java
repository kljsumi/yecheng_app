package com.hnu.highlynewapp.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import java.io.Serializable;

@ApiModel("登录成功返回信息")
public class UserVo implements Serializable{
    @ApiModelProperty("用户ID")
    private long uid;
    @ApiModelProperty("使用uuid生成的token")
    private String token;
    @ApiModelProperty("此用户认证的类型 0 管理员  1 普通用户")
    private Integer type;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
