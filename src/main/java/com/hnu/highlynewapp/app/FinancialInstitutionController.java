package com.hnu.highlynewapp.app;

import com.hnu.highlynewapp.entity.EnsureMethodIndexEntity;
import com.hnu.highlynewapp.entity.FinancialInstitutionEntity;
import com.hnu.highlynewapp.entity.InvestingNeedEntity;
import com.hnu.highlynewapp.exception.BizCodeEnum;
import com.hnu.highlynewapp.service.FinancialInstitutionService;
import com.hnu.highlynewapp.service.InvestingNeedService;

import com.hnu.highlynewapp.utils.R;
import com.hnu.highlynewapp.vo.InvestingSearchByParamVo;
import com.hnu.highlynewapp.vo.FinancialInstitutionVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author LengKuBaiCai
 * @Date
 */


@RestController
@RequestMapping("/financialInstitution")
@Api(value = "金融机构api管理",tags = "金融机构api管理")
public class FinancialInstitutionController {

    @Autowired
    private FinancialInstitutionService financialInstitutionService;
    @Autowired
    private InvestingNeedService investingNeedService;

    /**
     * 申请金融机构表认证
     */

    /**
     * 申请金融机构表认证
     */
    @ApiOperation("申请金融机构表认证")
    @PostMapping("/request")
    public R publish(@Validated @RequestBody FinancialInstitutionEntity financialInstitutionEntity) {
        financialInstitutionService.request(financialInstitutionEntity);
        long FinancialInstitutionId = financialInstitutionEntity.getFinancialInstitutionId();
        return R.ok().put("data", FinancialInstitutionId);
    }


    /**
     * 根据注册id获取对应的金融机构认证信息表
     */
    @ApiOperation("根据注册ID获取对应的金融机构认证信息表")
    @GetMapping("/getBygetByFinancialInstitutionId/{financial_institution_id}")
    public R getById(@PathVariable("financial_institution_id") Long financial_institution_id){
        List<FinancialInstitutionEntity> list = financialInstitutionService.getListById(financial_institution_id);
        if (list == null) {
            return R.error(BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getCode(), BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getMessage());
        }
        return R.ok().put("data", list);
    }

    /**
     * 根据企业ID获取对应的投资需求列表
     */
    @ApiOperation("根据企业ID获取对应的金融机构认证信息列表")
    @GetMapping("/getInvestListByFinancialInstitutionId/{financial_institution_id}")
    public R getByFinancialInstitutionId(@PathVariable("financial_institution_id") long financial_institution_id){
        List<InvestingNeedEntity> list = investingNeedService.getByFinancialInstitutionId(financial_institution_id);
        if(list == null) {
            return R.error(BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getCode(), BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getMessage());
        }
        return R.ok().put("data", list);
    }

/**
 * 根据公司名字模糊查询公司列表
 * @return
 */
    @ApiOperation("根据公司名字模糊查询公司列表")
    @GetMapping("/getFinancialInstitutionListByFinancialInstitutionName/{financial_institution_name}")
    public R getInvestListByFinancialInstitutionName(@PathVariable("financial_institution_name") String financial_institution_name) {
        List<FinancialInstitutionEntity> list = financialInstitutionService.getByFinancialInstitutionName(financial_institution_name);
        //new QueryWrapper<TalentNeedEntity>().eq("business_id", role_id).orderByDesc("publish_time")
        return R.ok().put("data", list);
    }
    /**
     * 修改金融机构认证表
     */
    @ApiOperation("修改金融机构认证表")
    @GetMapping("/updateFinancialInstitution")
    public R updateFinancialInstitution(@Validated @RequestBody FinancialInstitutionEntity financialInstitutionEntity){
        int flag = financialInstitutionService.updateFinancialInstitution(financialInstitutionEntity);
        return R.ok().put("data", flag);
    }

    /**
     * 根据注册id获取对应的金融机构认证的审核情况
     */
    @ApiOperation("根据注册id获取对应的金融机构认证的审核情况")
    @GetMapping("/getResultByFinancialInstitutionId/{financial_institution_id}")
    public R getResultByFinancialInstitutionId(@PathVariable("financial_institution_id") Long financial_institution_id){
        String result = financialInstitutionService.getResultByFinancialInstitutionId(financial_institution_id);
        return R.ok().put("data", result);
    }
    /**
     * 根据公司名字询公司信用材料
     * @return
     */
    @ApiOperation("根据公司名字查询公司列表")
    @GetMapping("/getCreditMaterialsByFinancialInstitutionName/{financial_institution_name}")
    public R getCreditMaterialsByFinancialInstitutionName(@PathVariable("financial_institution_name") String financial_institution_name) {
        List<FinancialInstitutionEntity> list = financialInstitutionService.getCreditMaterialsByFinancialInstitutionName(financial_institution_name);
        return R.ok().put("data", list);
    }



}
