package com.hnu.highlynewapp.app;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hnu.highlynewapp.entity.ContactEntity;
import com.hnu.highlynewapp.entity.PeopleRequireEachPositionEntity;
import com.hnu.highlynewapp.entity.TechnologyAchievementEntity;
import com.hnu.highlynewapp.exception.BizCodeEnum;
import com.hnu.highlynewapp.service.ContactService;
import com.hnu.highlynewapp.utils.Query;
import com.hnu.highlynewapp.vo.*;
import io.swagger.annotations.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.hnu.highlynewapp.entity.TalentSupplyEntity;
import com.hnu.highlynewapp.service.TalentSupplyService;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.utils.R;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 人才供应
 *
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:56
 */
@RestController
@RequestMapping("/talentsupply")
@Api(tags = "人才api管理")
public class TalentSupplyController {
    @Autowired
    private TalentSupplyService talentSupplyService;

    /**
     * 发布人才供应
     */
    @PostMapping("/publish")
    @ApiOperation("发布人才供应")
    public R publish(@Validated @RequestBody PublishTalentSupplyVo vo, BindingResult result) {

         Map<String,String> map = new HashMap<>();

         if (result.hasErrors()) {
             //获取效验错误结果
             result.getFieldErrors().forEach((item)-> {
                 //获取到错误提示
                 String message = item.getDefaultMessage();
                 //获取错误的属性的名字
                 String field = item.getField();
                 map.put(field,message);
             });
             return R.error(BizCodeEnum.INPUT_INVALID.getCode(), BizCodeEnum.INPUT_INVALID.getMessage()).put("data",map);
         } else {
             talentSupplyService.publish(vo);
             return R.ok();
         }

    }
    /**
     * 根据检索条件获取对应的人才供应列表
     */
    @ApiOperation("根据检索条件获取对应的人才供应列表")
    @GetMapping("/getlist")
    public R getList(){
        List<TalentSupplyEntity> list = talentSupplyService.getListByParma();
        return R.ok().put("data", list);
    }

    /**
     * 根据id获取人才信息
     */
    @ApiOperation("根据id获取人才信息")
    @GetMapping("/get/{talentSupplyId}")
    public R getById(@PathVariable("talentSupplyId") Long talentSupplyId){
        TalentSupplyEntity talentSupplyEntity = talentSupplyService.getById(talentSupplyId);
        if (talentSupplyEntity == null) {
            return R.error(BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getCode(), BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getMessage());
        }
        return R.ok().put("data", talentSupplyEntity);
    }

    /**
     * 根据角色id获取自己发布的人才信息
     * @return
     */
    @ApiOperation("根据角色id获取自己发布的人才信息")
    @GetMapping("/getlist/{role_id}")
    public R getListByRoleId(@ApiParam(value = "角色id",required = true) @PathVariable("role_id") Long role_id) {
        List<TalentSupplyEntity> list = talentSupplyService.list(new QueryWrapper<TalentSupplyEntity>().eq("school_id", role_id).orderByDesc("publish_time"));
        return R.ok().put("data", list);
    }

    /**
     * 根据发布时间降序获取人才信息
     * @return
     */
    @ApiOperation("根据发布时间降序获取人才信息")
    @GetMapping("/list_timeDesc")
    public R getListByPublishTimeDesc() {
        List<TalentSupplyEntity> list = talentSupplyService.list(new QueryWrapper<TalentSupplyEntity>().orderByDesc("publish_time"));
        return R.ok().put("data", list);
    }
    /**
     * 根据发布时间升序获取人才信息
     * @return
     */
    @ApiOperation("根据发布时间升序获取人才信息")
    @GetMapping("/list_timeAes")
    public R getListByPublishTimeAsc() {
        List<TalentSupplyEntity> list = talentSupplyService.list(new QueryWrapper<TalentSupplyEntity>().orderByAsc("publish_time"));
        return R.ok().put("data", list);
    }
    /**
     * 列表
     */
    @ApiIgnore
    @RequestMapping("/list")
    //@RequiresPermissions("highlynewapp:talentsupply:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = talentSupplyService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiIgnore
    @RequestMapping("/info/{talentSupplyId}")
    //@RequiresPermissions("highlynewapp:talentsupply:info")
    public R info(@PathVariable("talentSupplyId") Long talentSupplyId){
		TalentSupplyEntity talentSupply = talentSupplyService.getById(talentSupplyId);

        return R.ok().put("talentSupply", talentSupply);
    }

    /**
     * 保存
     */
    @ApiOperation("保存人才信息")
    @PostMapping ("/save")
    //@RequiresPermissions("highlynewapp:talentsupply:save")
    public R save(@RequestBody TalentSupplyEntity talentSupply){
		talentSupplyService.save(talentSupply);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation("根据id修改人才信息")
    @PostMapping("/update")
    //@RequiresPermissions("highlynewapp:talentsupply:update")
    public R update(@RequestBody TalentSupplyEntity talentSupply){
		talentSupplyService.updateById(talentSupply);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation("根据id删除人才信息")
    @PostMapping("/delete")
    //@RequiresPermissions("highlynewapp:talentsupply:delete")
    public R delete(@RequestBody Long[] talentSupplyIds){
		talentSupplyService.removeByIds(Arrays.asList(talentSupplyIds));

        return R.ok();
    }
    /**
     * 根据时间和行业领域检索
     * @param vo
     * @return
     */
    @PostMapping("/listByDurationDegree")
    @ApiOperation("根据时间和学历查询技术成果")
    @ApiResponses({
            @ApiResponse(code = 0,message = "success",response = TalentSupplyEntity.class)
    })
    public R listByCondition(@RequestBody DurationDegreeVo vo) {
        return R.ok().put("data",talentSupplyService.listByCondition(vo.getDuration(), vo.getDegree()));
    }

    /**
     * 高校审核简历列表
     * @param vo
     * @return
     */
    @PostMapping("/listByCheck")
    @ApiOperation("高校审核简历列表")
    @ApiResponses({
            @ApiResponse(code = 0,message = "success",response = TalentSupplyEntity.class)
    })
    public R listByCheckId(@RequestBody SchoolCheckVo vo) {
        return R.ok().put("data",talentSupplyService.listByCheckId(vo.getCheckId()));
    }

    /**
     * 高校审核简历列表
     * @param vo
     * @return
     */
    @PostMapping("/listByCondition")
    @ApiOperation("名字和技能搜索框查询")
    @ApiResponses({
            @ApiResponse(code = 0,message = "success",response = TalentSupplyEntity.class)
    })
    public R listByCondition(@RequestBody QueryListVo vo) {
        return R.ok().put("data",talentSupplyService.getConditionList(vo.getCondition()));
    }


    /**
     * 获取人才数据饼图
     */
    @ApiOperation("根据检索条件获取人才数据饼图")
    @RequestMapping("/getTalentDataPieChartList")
    @ResponseBody
    public R getTalentDataPieChartList() {
        List<PeopleRequireEachPositionEntity> talentDataPieChartList = talentSupplyService.getTalentDataPieChartList();
        return R.ok().put("data", talentDataPieChartList);
    }

}


