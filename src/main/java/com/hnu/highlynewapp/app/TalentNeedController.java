package com.hnu.highlynewapp.app;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hnu.highlynewapp.entity.CompanySizeDataEntity;
import com.hnu.highlynewapp.entity.PeopleRequireEachPositionEntity;
import com.hnu.highlynewapp.entity.TalentNeedEntity;
import com.hnu.highlynewapp.entity.TalentSupplyEntity;
import com.hnu.highlynewapp.exception.BizCodeEnum;
import com.hnu.highlynewapp.service.TalentNeedService;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.utils.R;
import com.hnu.highlynewapp.utils.ValidationUtils;
import com.hnu.highlynewapp.vo.DurationDegreeVo;
import com.hnu.highlynewapp.vo.PublishTalentNeedVo;
import com.hnu.highlynewapp.vo.QueryListVo;
import com.hnu.highlynewapp.vo.SchoolCheckVo;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 人才需求
 *
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:56
 */
@Api(tags = "人才api管理")
@RestController
@RequestMapping("/talentneed")
public class TalentNeedController {
    @Autowired
    private TalentNeedService talentNeedService;

    /**
     * 发布人才需求
     */
    @ApiOperation("发布人才需求")
    @PostMapping("/publish")
    public R publish(@Validated @RequestBody PublishTalentNeedVo vo, BindingResult result) {
        if (result.hasErrors()) {
            Map<String, String> map = ValidationUtils.getErrors(result);
            return R.error(BizCodeEnum.INPUT_INVALID.getCode(), BizCodeEnum.INPUT_INVALID.getMessage()).put("data",map);
        } else {
            talentNeedService.publish(vo);
            return R.ok();
        }
    }

    /**
     * 根据检索条件获取对应的人才需求列表
     */
    @ApiOperation("根据检索条件获取对应的人才需求列表")
    @GetMapping("/getlist")
    public R getList(){
        List<TalentNeedEntity> list = talentNeedService.getListByParma();
        return R.ok().put("data", list);
    }

    /**
     * 根据id获取人才需求信息
     */
    @ApiOperation("根据id获取人才需求信息")
    @GetMapping("/get/{talentNeedId}")
    public R getById(@PathVariable("talentNeedId") Long talentNeedId){
        TalentNeedEntity talentNeedEntity = talentNeedService.getById(talentNeedId);
        if (talentNeedEntity == null) {
            return R.error(BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getCode(), BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getMessage());
        }
        return R.ok().put("data", talentNeedEntity);
    }

    /**
     * 根据公司id获取自己发布的人才需求信息
     * @return
     */
    @ApiOperation("根据公司id获取自己发布的人才需求信息")
    @GetMapping("/getlist/{role_id}")
    public R getListByRoleId(@ApiParam(value = "角色id",required = true) @PathVariable("role_id") Long role_id) {
        List<TalentNeedEntity> list = talentNeedService.list(new QueryWrapper<TalentNeedEntity>().eq("business_id", role_id).orderByDesc("publish_time"));
        return R.ok().put("data", list);
    }


    /**
     * 根据发布时间降序获取人才信息
     * @return
     */
    @ApiOperation("根据发布时间降序获取人才信息")
    @GetMapping("/list_timeDesc")
    public R getListByPublishTimeDesc() {
        List<TalentNeedEntity> list = talentNeedService.list(new QueryWrapper<TalentNeedEntity>().orderByDesc("publish_time"));
        return R.ok().put("data", list);
    }
    /**
     * 根据发布时间升序获取人才信息
     * @return
     */
    @ApiOperation("根据发布时间升序获取人才信息")
    @GetMapping("/list_timeAes")
    public R getListByPublishTimeAsc() {
        List<TalentNeedEntity> list = talentNeedService.list(new QueryWrapper<TalentNeedEntity>().orderByAsc("publish_time"));
        return R.ok().put("data", list);
    }
    /**
     * 列表
     */
    @ApiIgnore
    @RequestMapping("/list")
    //@RequiresPermissions("highlynewapp:talentneed:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = talentNeedService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiIgnore
    @RequestMapping("/info/{talentNeedId}")
    //@RequiresPermissions("highlynewapp:talentneed:info")
    public R info(@PathVariable("talentNeedId") Long talentNeedId){
		TalentNeedEntity talentNeed = talentNeedService.getById(talentNeedId);

        return R.ok().put("talentNeed", talentNeed);
    }

    /**
     * 保存
     */
    @ApiIgnore
    @RequestMapping("/save")
    //@RequiresPermissions("highlynewapp:talentneed:save")
    public R save(@RequestBody TalentNeedEntity talentNeed){
		talentNeedService.save(talentNeed);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiIgnore
    @RequestMapping("/update")
    //@RequiresPermissions("highlynewapp:talentneed:update")
    public R update(@RequestBody TalentNeedEntity talentNeed){
		talentNeedService.updateById(talentNeed);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiIgnore
    @RequestMapping("/delete")
    //@RequiresPermissions("highlynewapp:talentneed:delete")
    public R delete(@RequestBody Long[] talentNeedIds){
		talentNeedService.removeByIds(Arrays.asList(talentNeedIds));

        return R.ok();
    }


    /**
     * 根据时间和最低学历检索
     * @param vo
     * @return
     */
    @PostMapping("/listByDurationDegree")
    @ApiOperation("根据时间和学历领域检索")
    @ApiResponses({
            @ApiResponse(code = 0,message = "success",response = TalentNeedEntity.class)
    })
    public R listByCondition(@RequestBody DurationDegreeVo vo) {
        return R.ok().put("data",talentNeedService.listByCondition(vo.getDuration(), vo.getDegree()));
    }

    /**
     * 高校审核简历列表
     * @param vo
     * @return
     */
    @PostMapping("/listByCheck")
    @ApiOperation("高校审核简历列表")
    @ApiResponses({
            @ApiResponse(code = 0,message = "success",response = TalentNeedEntity.class)
    })
    public R listByCheckId(@RequestBody SchoolCheckVo vo) {
        return R.ok().put("data",talentNeedService.listByCheckId(vo.getCheckId()));
    }

    /**
     * 高校审核简历列表
     * @param vo
     * @return
     */
    @PostMapping("/listByCondition")
    @ApiOperation("名字和技能搜索框查询")
    @ApiResponses({
            @ApiResponse(code = 0,message = "success",response = TalentNeedEntity.class)
    })
    public R listByCheckId(@RequestBody QueryListVo vo) {
        return R.ok().put("data",talentNeedService.getConditionList(vo.getCondition()));
    }

    /**
     * 获取公司数据饼图
     */
    @ApiOperation("根据检索条件获取公司数据饼图")
    @RequestMapping("/getCompanySizeDataList")
    @ResponseBody
    public R getCompanySizeDataList() {
        List<PeopleRequireEachPositionEntity> numberOfPeopleRequiredEachPosition = talentNeedService.getNumberOfPeopleRequiredEachPosition();
        return R.ok().put("data", numberOfPeopleRequiredEachPosition);
    }


}
