package com.hnu.highlynewapp.app;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.hnu.highlynewapp.entity.BusinessInfoEntity;
import com.hnu.highlynewapp.exception.BizCodeEnum;
import com.hnu.highlynewapp.service.ContactService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hnu.highlynewapp.service.BusinessInfoService;
import com.hnu.highlynewapp.utils.PageUtils;
import com.hnu.highlynewapp.utils.R;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 企业信息
 *
 * @author hnu_worker
 * @email 825963704@qq.com
 * @date 2021-01-03 10:35:57
 */
@Api(tags = "角色api管理")
@RestController
@RequestMapping("/businessinfo")
public class BusinessInfoController {
    @Autowired
    private BusinessInfoService businessInfoService;


    /**
     * 获取所有高新企业
     * @return
     */
    @ApiOperation("获取所有高新企业")
    @GetMapping("/getlist")
    public R getList() {
        List<BusinessInfoEntity> list = businessInfoService.list();

        return R.ok().put("data", list);
    }

    /**
     * 根据id获取高新企业
     * @return
     */
    @ApiOperation("根据id获取高新企业")
    @GetMapping("/get/{businessInfoId}")
    public R getById(@PathVariable("businessInfoId") Long businessInfoId) {
        BusinessInfoEntity businessInfoEntity = businessInfoService.getById(businessInfoId);
        if (businessInfoEntity == null) {
            return R.error(BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getCode(), BizCodeEnum.INFO_NOT_FOUND_EXCEPTION.getMessage());
        }
        return R.ok().put("data", businessInfoEntity);
    }

}
