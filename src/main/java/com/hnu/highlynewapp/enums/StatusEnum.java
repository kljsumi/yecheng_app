package com.hnu.highlynewapp.enums;

public enum StatusEnum {
    STATU_ONE(1,"正在审核"),
    STATU_TWO(2,"审核通过"),
  STATU_THREE(3,"审核不通过" )
    ;
    private int code;
    private String message;
    StatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }
    public int getCode() {
        return code;
    }


    public String getMessage() {
        return message;
    }
}
