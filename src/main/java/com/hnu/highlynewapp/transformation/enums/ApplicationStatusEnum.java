package com.hnu.highlynewapp.transformation.enums;

public enum ApplicationStatusEnum {
    STATUS_ZERO(0, "未审核"),
    STATUS_ONE(1, "正在审核"),
    STATUS_TWO(2, "审核通过"),
    STATUS_THREE(3, "审核失败");
    private int code;
    private String message;

    ApplicationStatusEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public static Integer findCodeByMsg(String message) {
        Integer code = null;
        for (ApplicationStatusEnum statusEnum : ApplicationStatusEnum.values()) {
            if (statusEnum.getMessage().equals(message))
                code = statusEnum.getCode();
        }
        return code;
    }
}
