package com.hnu.highlynewapp.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author kLjSumi
 * @Date 2021/1/3
 */
@ConfigurationProperties(prefix = "highlynewapp.oss")
@Data
@Component
public class OssConfigProperties {
    private Integer coreSize;
    private Integer maxSize;
    private Integer keepAliveTime;
}
